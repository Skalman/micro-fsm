/**
 * A function that takes a single argument.
 */
type AnyFunction = (...args: any) => any;

interface EventConfig<TState extends string, TFn extends AnyFunction> {
  /** Optional array of strings describing from which states this event can be run. */
  from?: readonly TState[];

  /** Optional string describing the next state after running this event. */
  to?: TState;

  /** Optional function to be executed when this event is run. */
  fn?: TFn;
}

type FiniteStateMachine2<TState, TEvents> = {
  /** Get the current state */
  readonly current: TState;

  /** Get the previous state */
  readonly prev: TState;

  /** Returns whether it's possible to run this event. */
  can(eventName: keyof TEvents): boolean;
} & TEvents;

type ForbiddenEventNames = "current" | "prev" | "can";

export type FiniteStateMachine<TState, TConfig extends Config<string>> = [
  keyof TConfig & ForbiddenEventNames,
] extends [never]
  ? FiniteStateMachine2<
      TState | SimplifyUnion<GetStateFromConfig<TConfig>>,
      GetEventsFromConfig<TConfig>
    >
  : { "ERROR: Events must not be named 'current', 'prev' or 'can'.": never };

type GetStateFromEventConfig<T> =
  | (T extends { from: (infer TStateFrom)[] } ? TStateFrom : never)
  | (T extends { to: infer TStateTo } ? TStateTo : never);

type GetStateFromConfig<TConfig extends Config<string>> =
  GetStateFromEventConfig<TConfig[keyof TConfig]>;

type GetEventsFromConfig<T extends Config<string>> = {
  [K in keyof T]: T[K] extends { fn: infer F } ? F : () => void;
};

interface Config<TState extends string> {
  [eventName: string]: EventConfig<TState, AnyFunction>;
}

export interface FsmFunction {
  /**
   * Create a finite state machine.
   * @param initialState The initial state of the finite state machine.
   * @param config Object containing the events
   */
  <
    TState extends string,
    // `TState2` is needed to get TypeScript to infer more specific strings
    // inside of `TConfig`.
    TState2 extends string,
    TConfig extends Config<TState2>,
  >(
    initialState: TState,
    config: TConfig
  ): FiniteStateMachine<TState, TConfig>;
}

/** Make TypeScript evaluate the complex types proactively. */
type SimplifyUnion<T> = T extends string ? keyof { [K in T]: never } : never;

type Mutable<T> = {
  -readonly [P in keyof T]: T[P];
};

const fsm: FsmFunction = (
  // The user will call this function with the initial state. Reuse this
  // argument for the current state.
  current: string,
  config: Config<string>,
  // Declare variables as arguments for better minification.
  machine?: Mutable<FiniteStateMachine2<string, {}>>
) => {
  machine = {
    current,
    prev: current,
    can: (eventName: string) => {
      return config[eventName]?.from?.includes(current) ?? true;
    },
  };

  Object.entries(config)
    // Use `map` instead of `forEach` for better minification.
    .map(
      (
        [eventName, eventConfig],

        // Declare variables as arguments for better minification.
        returnValue
      ) => {
        // Return from this function for better minification.
        return ((machine as any)[eventName] = (...args: unknown[]) => {
          if (!machine.can(eventName as never)) {
            // Use template string for first variable but not second, for better
            // minification.
            throw `Can't ${eventName} from ` + current;
          } else {
            // If `event.fn()` is defined, then return its result, else return
            // undefined.
            returnValue = eventConfig.fn?.(...args);

            if (eventConfig.to) {
              machine.prev = current;
              machine.current = current = eventConfig.to;
            }

            return returnValue;
          }
        });
      }
    );

  return machine as any;
};

export default fsm;
