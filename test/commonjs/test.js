// @ts-check
const fsm = require("micro-fsm");

const machine = fsm("stopped", {
  start: { from: ["stopped"], to: "started" },
  stop: { from: ["started"], to: "stopped" },
});
