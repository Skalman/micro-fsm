#!/bin/bash

rm -r build/*

# Generates build/lib.js and build/lib.d.ts
npx tsc --project tsconfig.json

npx uglifyjs --compress --mangle --module -- build/lib.js \
    | tr -d '\n' \
    | sed 's/;$//' \
    > build/lib.mjs

# Export it both as default and as single for easier usage
echo -n 'f=module.exports=' > build/lib.js
cat build/lib.mjs \
    | sed 's/export default//' \
    >> build/lib.js
echo -n ',f.default=f' >> build/lib.js

cp package-real.json build/package.json
cp README.md build/
